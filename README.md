## Prometheus для сбора и хранения метрик

Сообщения отправляются в telegram, ИД чата и токен бота необходимо указать в файле .env

В файле prometheus/prometheus.yml и prometheus/alert.rules необходимо указать <TARGET_URL> (url приложения)

## Импорт дашборда

* В меню перейдите в Dashboards
* Нажмите кнопку New
* Выберите Import
* Upload a dashboard JSON file
* Выберите файл grafana/Skillfactory Diplom-1691868024087.json
* Нажмие Load
